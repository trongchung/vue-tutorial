var vueInstance = new Vue({
    el: '#app',
    data:{
        title:'áo thun cao cấp',
        url: 'https://www.youtube.com/watch?v=iWcApeci_g0&list=PLv6GftO355AtDjStqeyXvhA1oRLuhvJWf&index=18',
        blank:'_blank',
        prodSelect: 0,
        price: 300000,
        sale: 0.2,
        cartNumber:2,
        listColor:[
            {
                image:'img/red.jpg',
                quantity:4,
                txtColor: 'Màu đỏ',
            },
            {
                image:'img/black.jpg',
                quantity:0,
                txtColor: 'Màu đen',
            },
            {
                image:'img/blue.jpg',
                quantity:3,
                txtColor: 'Màu xanh',
            }

        ]
    },
    methods:{
        selectColor(index) {
            this.prodSelect = index;
            // console.log(e, index, this);
        },
        addCart(){

            if (this.cartNumber + 1 > this.getProduct.quantity){
                alert('het hang');
            }else {
                this.cartNumber = this.cartNumber + 1;
            }
        },
        classActive(index) {
            return {
                active: this.prodSelect === index
            }
        },
    },
    computed:{
        priceOld() {
            var number1 = this.price;
            return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(number1);
        },
        priceNew() {
            var number2 = this.price - this.price*this.sale;
            return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(number2);
        },
        getProduct(){
            var index = this.prodSelect;
            return this.listColor[index];
        }
    }
});